import firebase from 'firebase/app'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: 'AIzaSyB1NW0SU0bbbdIQ6jAGuxJcII0-nUY_Ee8',
  authDomain: 'qwitter-44e8a.firebaseapp.com',
  projectId: 'qwitter-44e8a',
  storageBucket: 'qwitter-44e8a.appspot.com',
  messagingSenderId: '457858258226',
  appId: '1:457858258226:web:8f3c1f1f0b18e7b8dcb32b'
}

// Initialize Firebase
firebase.initializeApp(firebaseConfig)
const db = firebase.firestore()

export default db
